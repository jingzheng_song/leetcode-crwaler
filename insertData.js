/**
 * Insert problems data into database using backend api. Problems data is mannually crawled.
 */
var request = require('request');
const fs = require('fs');

const problemUrl = 'https://leetcode.com/problems/$slug/description/';
const categories = ['algorithms', 'shell', 'database', 'concurrency'];
categories.forEach(c => {
    const fileName = "./data/" + c + ".json";
    let rawdata = fs.readFileSync(fileName);
    let json = JSON.parse(rawdata);
    saveProblems(json);
})

function saveProblems(json) {
    json.stat_status_pairs.map(function (p) {
        return {
            id: p.stat.question_id,
            articleLive: p.stat.question__article__live,
            f: p.stat.question__f,
            title: p.stat.question__title,
            titleSlug: p.stat.question__title_slug,
            hide: p.stat.question__hide,
            totalAcs: p.stat.total_acs,
            totalSubmitted: p.stat.total_submitted,
            category: json.category_slug,
            fid: p.stat.frontend_question_id,
            isNewQuestion: p.stat.is_new_question,
            difficulty: p.difficulty.level - 1,
            paidOnly: p.paid_only,
            frequency: p.frequency,
            link: problemUrl.replace('$slug', p.stat.question__title_slug),
        };
    }).forEach(problem => request.post("http://127.0.0.1:8080/questions", { json: problem }));
}


