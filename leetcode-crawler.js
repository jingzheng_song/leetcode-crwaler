/**
 * Crawl from leetcode with login, currently this script does not work for leetcode.com due to new protection.
 */
var request = require('request');

const loginUrl = 'https://leetcode.com/accounts/login/';
const base = 'https://leetcode.com';
const problemsUrl = 'https://leetcode.com/api/problems/$category/';
const problemUrl = 'https://leetcode.com/problems/$slug/description/';
const graphql = 'https://leetcode.com/graphql';

// const loginUrl = 'https://leetcode-cn.com/accounts/login/';
// const base = 'https://leetcode-cn.com';
// const problemsUrl = 'https://leetcode-cn.com/api/problems/$category/';
// const problemUrl = 'https://leetcode-cn.com/problems/$slug/description/';
// const graphql = 'https://leetcode-cn.com/graphql';

const categories = [
    'algorithms',
    'database',
    'shell'
];

const levelToName = function(level) {
    switch (level) {
      case 1:  return 'Easy';
      case 2:  return 'Medium';
      case 3:  return 'Hard';
      default: return ' ';
    }
  };

const makeOpts = function (url, user) {
    const opts = {};
    opts.url = url;
    opts.headers = {};
    opts.headers.Cookie = 'LEETCODE_SESSION=' + user.sessionId +
        ';csrftoken=' + user.sessionCSRF + ';';
    opts.headers['X-CSRFToken'] = user.sessionCSRF;
    opts.headers['X-Requested-With'] = 'XMLHttpRequest';
    return opts;
};

const getSetCookieValue = function (resp, key) {
    const cookies = resp.headers['set-cookie'];
    if (!cookies) return null;

    for (let i = 0; i < cookies.length; ++i) {
        const sections = cookies[i].split(';');
        for (let j = 0; j < sections.length; ++j) {
            const kv = sections[j].trim().split('=');
            if (kv[0] === key) return kv[1];
        }
    }
    return null;
};

const checkError = function (e, resp, expectedStatus) {
    if (!e && resp && resp.statusCode !== expectedStatus) {
        const code = resp.statusCode;
        console.debug('http error: ' + code);

        if (code === 403 || code === 401) {
            e = {
                msg: 'session expired, please login again',
                statusCode: -1
            };
        } else {
            e = { msg: 'http error', statusCode: code };
        }
    }
    return e;
};

let signin = function (user, cb) {
    console.debug('running leetcode.signin');
    request(loginUrl, function (e, resp, body) {
        e = checkError(e, resp, 200);
        if (e) return cb(e);

        user.loginCSRF = getSetCookieValue(resp, 'csrftoken');

        const opts = {
            url: loginUrl,
            headers: {
                Origin: base,
                Referer: loginUrl,
                Cookie: 'csrftoken=' + user.loginCSRF + ';'
            },
            form: {
                csrfmiddlewaretoken: user.loginCSRF,
                login: user.login,
                password: user.pass,
                recaptcha_token: ''
            }
        };
        request.post(opts, function (e, resp, body) {
            if (e) return cb(e);
            if (resp.statusCode !== 302) return cb('invalid password?');

            user.sessionCSRF = getSetCookieValue(resp, 'csrftoken');
            user.sessionId = getSetCookieValue(resp, 'LEETCODE_SESSION');
            //   session.saveUser(user);
            return cb(null, user);
        });
    });
};

const getCategoryProblems = function (category, user, cb) {
    console.debug('running leetcode.getCategoryProblems: ' + category);
    const opts = makeOpts(problemsUrl.replace('$category', category), user);

    request(opts, function (e, resp, body) {
        e = checkError(e, resp, 200);
        if (e) return cb(e);

        const json = JSON.parse(body);

        // leetcode permits anonymous access to the problem list
        // while we require login first to make a better experience.
        if (json.user_name.length === 0) {
            console.debug('no user info in list response, maybe session expired...');
            return cb(EXPIRED);
        }

        const problems = json.stat_status_pairs
            .map(function (p) {
                return {
                    id: p.stat.question_id,
                    articleLive: p.stat.question__article__live,
                    f: p.stat.question__f,
                    title: p.stat.question__title,
                    titleSlug: p.stat.question__title_slug,
                    hide: p.stat.question__hide,
                    totalAcs: p.stat.total_acs,
                    totalSubmitted: p.stat.total_submitted,
                    category: json.category_slug,
                    fid: p.stat.frontend_question_id,
                    isNewQuestion: p.stat.is_new_question,
                    difficulty: p.difficulty.level - 1,
                    paidOnly: p.paid_only,
                    frequency: p.frequency,
                    link: problemUrl.replace('$slug', p.stat.question__title_slug),
                };
            });

        return cb(null, problems);
    });
};

const getProblems = function (user, cb) {
    console.debug('running leetcode.getProblems');
    let problems = [];
    categories.forEach(category => getCategoryProblems(category, user, cb))

};

const getProblem = function(problem, user, cb) {
    console.debug('running leetcode.getProblem');
  
    const opts = makeOpts(graphql, user);
    opts.headers.Origin = base;
    opts.headers.Referer = problem.link;
  
    opts.json = true;
    opts.body = {
      query: [
        'query getQuestionDetail($titleSlug: String!) {',
        '  question(titleSlug: $titleSlug) {',
        '    content',
        '    stats',
        '    codeDefinition',
        '    sampleTestCase',
        '    enableRunCode',
        '    metaData',
        '    translatedContent',
        '  }',
        '}'
      ].join('\n'),
      variables:     {titleSlug: problem.slug},
      operationName: 'getQuestionDetail'
    };
  
    request.post(opts, function(e, resp, body) {
      e = checkError(e, resp, 200);
      if (e) return cb(e);
  
      const q = body.data.question;
      if (!q) return cb('failed to load problem!');
  
      problem.totalAC = JSON.parse(q.stats).totalAccepted;
      problem.totalSubmit = JSON.parse(q.stats).totalSubmission;
  
      let content = q.translatedContent ? q.translatedContent : q.content;
      // Replace <sup/> with '^' as the power operator
      content = content.replace(/<\/sup>/gm, '').replace(/<sup>/gm, '^');
      problem.desc = he.decode(cheerio.load(content).root().text());
  
      problem.templates = JSON.parse(q.codeDefinition);
      problem.testcase = q.sampleTestCase;
      problem.testable = q.enableRunCode;
      problem.templateMeta = JSON.parse(q.metaData);
      // @si-yao: seems below property is never used.
      //problem.discuss =  q.discussCategoryId;
  
      return cb(null, problem);
    });
  };

const cb = function(message, res) {
    res.forEach(p => request.post("http://127.0.0.1:8080/questions", {json: p}));
    console.log(res);
}

signin({ login: 'liutuo091@gmail.com', pass: 'F=fxYwDfXANKBTE4' }, (m, user) => getProblems(user, cb));
